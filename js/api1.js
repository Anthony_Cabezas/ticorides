window.onload = obtProvincia;

document.querySelector('#provincia1').addEventListener('click',function(){
    obtCanton();
});

document.querySelector('#canton1').addEventListener('click',function(){
    obtDistrito();
});

function obtProvincia(){
    var webProvincia='https://ubicaciones.paginasweb.cr/provincias.json';
    traerDatos(webProvincia,'provincia1');    
}


function obtCanton(){
    var idProvincia=document.getElementById('provincia1').value;
    var webCanton='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/cantones.json'

    traerDatos(webCanton,'canton1');    
}

function obtDistrito(){
    var idProvincia=document.getElementById('provincia1').value;
    var idCanton=document.getElementById('canton1').value;
    var webDistrito='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/canton/'+idCanton+'/distritos.json'

    traerDatos(webDistrito,'distrito1');    
}

function traerDatos(purl,ptag){
    fetch(purl)
    .then((respuesta)=>{
        return respuesta.json();
    }).then((respuesta)=>{
        Limpiar(ptag);
        let resultado= document.querySelector('#'+ptag);
            for (var clave in respuesta){
                if (respuesta.hasOwnProperty(clave)) {
                    resultado.innerHTML+="<option value='"+clave+"'>"+respuesta[clave]+"</option>";
                        
                }
            }

    })
}

function Limpiar(tag)
{
    document.getElementById(tag).innerHTML = "";
}
