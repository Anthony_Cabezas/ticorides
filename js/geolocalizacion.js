window.onload = obtProvinvia;

document.querySelector('#provincia').addEventListener('click',function(){
    obtCanton();
});

document.querySelector('#canton').addEventListener('click',function(){
    obtDistrito();
});

function obtProvinvia(){
    var webProvincia='https://ubicaciones.paginasweb.cr/provincias.json';
    traerDatos(webProvincia,'provincia');    
}


function obtCanton(){
    var idProvincia=document.getElementById('provincia').value;
    var webCanton='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/cantones.json'

    traerDatos(webCanton,'canton');    
}

function obtDistrito(){
    var idProvincia=document.getElementById('provincia').value;
    var idCanton=document.getElementById('canton').value;
    var webDistrito='https://ubicaciones.paginasweb.cr/provincia/'+idProvincia+'/canton/'+idCanton+'/distritos.json'

    traerDatos(webDistrito,'distrito');    
}

function traerDatos(purl,ptag){
    fetch(purl)
    .then((respuesta)=>{
        return respuesta.json();
    }).then((respuesta)=>{
        Limpiar(ptag);
        let resultado= document.querySelector('#'+ptag);
            for (var clave in respuesta){
                if (respuesta.hasOwnProperty(clave)) {
                    resultado.innerHTML+="<option value='"+clave+"'>"+respuesta[clave]+"</option>";
                        
                }
            }

    })
}

function Limpiar(tag)
{
    document.getElementById(tag).innerHTML = "";
}
